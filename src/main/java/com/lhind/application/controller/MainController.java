package com.lhind.application.controller;


import com.lhind.application.model.Request;
import com.lhind.application.model.UserDetails;
import com.lhind.application.service.adminService.AdminServiceIml;
import com.lhind.application.service.flightService.FlightServiceIml;
import com.lhind.application.service.requestService.RequestServiceIml;
import com.lhind.application.service.tripService.TripServiceIml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.math.BigInteger;
import java.util.List;

@Controller
public class MainController {


    @Autowired
    AdminServiceIml adminServiceIml;

    @Autowired
    TripServiceIml tripServiceIml;

    @Autowired
    RequestServiceIml requestServiceIml;

    @Autowired
    FlightServiceIml flightServiceIml;


    /**
     * Method that return view of dashboard
     * @param model
     * @return
     */
    @GetMapping("/")
    public String returnHome(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        if(auth.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))){
            BigInteger nrUsers=adminServiceIml.getNumberOfAllUsers();
            BigInteger nrTrips=tripServiceIml.getNumberOfTrips();
            BigInteger nrRequests=tripServiceIml.getNumberOfWaitingTrips();
            BigInteger nrFlights=flightServiceIml.getNumberOfAllFlights();

            model.addAttribute("nrUsers", nrUsers);
            model.addAttribute("nrTrips", nrTrips);
            model.addAttribute("nrRequests", nrRequests);
            model.addAttribute("nrFlights", nrFlights);
            return "adminDashboard";
        } else {

            BigInteger nrApprovedTrips=tripServiceIml.getNumberOfTripsByUserId(userId);
            BigInteger nrWaitingTrips=tripServiceIml.getNumberOfWaitingTripsByUserId(userId);
            BigInteger nrCreatedTrips=tripServiceIml.getNumberOfCreatedTripsByUserId(userId);
            BigInteger nrFlights=flightServiceIml.getNumberOfAllFlightsByUserId(userId);

            model.addAttribute("nrOfApprovedTrips", nrApprovedTrips);
            model.addAttribute("nrOfWaitingTrips", nrWaitingTrips);
            model.addAttribute("nrOfCreatedTrips", nrCreatedTrips);
            model.addAttribute("nrOfFlights", nrFlights);
            return "userDashboard";
        }


    }


}
