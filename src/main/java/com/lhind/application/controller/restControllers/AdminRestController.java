package com.lhind.application.controller.restControllers;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.lhind.application.model.*;
import com.lhind.application.modelMappings.UserMapper;
import com.lhind.application.modelMappings.barChartMapper;
import com.lhind.application.modelMappings.chartMapper;
import com.lhind.application.modelMappings.chartsTest;
import com.lhind.application.repository.adminRepository.AdminRepository;
import com.lhind.application.repository.adminRepository.AdminRepositoryIml;
import com.lhind.application.service.adminService.AdminServiceIml;
import com.lhind.application.service.flightService.FlightServiceIml;
import com.lhind.application.service.requestService.RequestService;
import com.lhind.application.service.requestService.RequestServiceIml;
import com.lhind.application.service.tripService.TripServiceIml;
import org.apache.catalina.User;
import org.apache.tomcat.util.json.JSONParser;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
/*import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;*/
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import java.sql.*;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;
import java.util.Date;


@RestController()
@RequestMapping("/admin/api")
public class AdminRestController {



    Logger logger= LoggerFactory.getLogger(AdminRestController.class);

    @Autowired
    EntityManager entityManager;


    @Autowired
    AdminServiceIml serviceIml;

    @Autowired
    TripServiceIml tripServiceIml;

    @Autowired
    RequestServiceIml requestServiceIml;

    @Autowired
    FlightServiceIml flightServiceIml;


    @Autowired
    JdbcTemplate template;




    /**
     * Method that return all users
     * @return
     */
    @GetMapping("/users")
    ResponseEntity<?> all() {
        logger.info("Admin has accessed Users View");
        List<UserDetails> lista =serviceIml.getAllUsers();
    if(lista==null){
        Map<String, String> test=new HashMap<>();
        test.put("message", "Lista eshte bosh");

        return new ResponseEntity<Map<String, String>>(test, HttpStatus.NOT_FOUND);
    } else {
        return  new ResponseEntity<List<UserDetails>>(lista, HttpStatus.OK);
    }

    }

    /**
     * Method that return an users
     * @return
     */
    @GetMapping("/users/getUserById/{id}")
    ResponseEntity<?> getUserById(@PathVariable Integer id) {

          UserDetails user = serviceIml.getUserById(id);
          if(user.getId()==null){
              return new ResponseEntity<String>("User not found", HttpStatus.NOT_FOUND);
          }

          return new ResponseEntity<UserDetails>(user, HttpStatus.OK);

    }

    /**
     * Method that return users searched by name
     * @return
     */
    @GetMapping("/users/searchUserByName/{name}")
    ResponseEntity<?> getAllByName(@PathVariable String name) {

            List<UserDetails> lista =serviceIml.searchUsers(name);
            if(lista.isEmpty()){
                return new ResponseEntity<String>("No result", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<List<UserDetails> >(lista, HttpStatus.OK);


    }

    /**
     * Method that return users searched by username
     * @return
     */
    @GetMapping("/users/searchUserByUsername/{username}")
    Boolean checkIfUserExistByUsername(@PathVariable String username) {

        /// exist =serviceIml.getUserByUsername(username);
        UserDetails exist =serviceIml.getUserByUsername(username);
        if(exist.getId()!=null){
            return true;
        } else {
            return false;
        }


    }

    /**
     * Method that save an user when request is made by REST API
     * @return
     */
    @Autowired
    PasswordEncoder passwordEncoder;

   @PostMapping("/users/addNewUser")
   public ResponseEntity<?> addNewUser(HttpServletRequest request){

       ///A map that will hold the errors
       Map<String, List<String>> errors = new HashMap<String, List<String>>();


       String firstName=request.getParameter("firstName");
       String lastName=request.getParameter("lastName");
       String gender=request.getParameter("gender");
       String birthday= request.getParameter("birthday");
       String personalID=request.getParameter("personalID");
       String email=request.getParameter("email");
       String username=request.getParameter("username");
       String password=request.getParameter("password");
       String passwordconfrimation=request.getParameter("confirmPassword");
       String role=request.getParameter("role");


      ///Check if firstName is empty
       if(firstName.isEmpty()){
           errors.put("firstName", Arrays.asList("First Name cannot be null"));
       }

       ///Check if lastName is empty
       if(lastName.isEmpty()){
           errors.put("lastName", Arrays.asList("Last Name cannot be null"));
       }

       ///Check if gender is empty
       if(gender.equals("Select Gender")){
           errors.put("gender", Arrays.asList("Gender cannot be null"));
       }

       ///Check if birthday is empty
       if(birthday.isEmpty()){
           errors.put("birthday", Arrays.asList("Birthday cannot be null"));
       }

       ///Check if personalID is empty
       if(personalID.isEmpty()){
           errors.put("personalID", Arrays.asList("Personal ID cannot be null"));
       }

       ///Check if email is empty
       if(email.isEmpty()){
           errors.put("email", Arrays.asList("Email cannot be null"));
       }

       ///Check if username is empty
       if(username.isEmpty()){
           errors.put("username", Arrays.asList("Username cannot be null"));
       }


       ///Search in database and get an User
       UserDetails user1 = serviceIml.getUserByUsername(username);

       ///So if user Id is not null user exist
       if(user1.getId()!=null){
           errors.put("username", Arrays.asList("Username already taken"));
       }

       ///Check if password is empty
       if(password.isEmpty()){
           errors.put("password", Arrays.asList("Password cannot be null"));
       }

       ///Check if password confirmation is empty
       if(passwordconfrimation.isEmpty()){
           errors.put("passwordConfirmation", Arrays.asList("Password Confrimation cannot be null"));
       }

       ///If passwords are not equal
       if(!password.equals(passwordconfrimation)){
           System.out.println("Password "+password);
           System.out.println("Password Confirmation: "+passwordconfrimation);
           errors.put("password",Arrays.asList("Password are not equal"));
           errors.put("passwordConfirmation",Arrays.asList("Password are not equal"));
       }

       ///If user have not select any role
       if(role.equals("Select Role")){
           errors.put("role", Arrays.asList("Role cannot be null"));
       }


       ///If map with errors is not empty return a response that will send in front all error messages for each field
       if(!errors.isEmpty()){
           return new ResponseEntity<Map<String, List<String>>>(errors, HttpStatus.BAD_REQUEST);
       }

       ///Everything is validated

       ///Convert birthdaty into date
       java.sql.Date birthday1= java.sql.Date.valueOf(request.getParameter("birthday"));


       ///Current timestamp
       Timestamp instant= Timestamp.from(Instant.now());

       ///Declare new UserDetails
       UserDetails user=new UserDetails();

       user.setFirstName(firstName);
       user.setLastName(lastName);
       user.setGender(gender.charAt(0));
       user.setIsEnabled(1);
       user.setCreated_date_time(instant);
       user.setBirthday(birthday1);
       user.setEmail(email);
       ///Encode the password
       user.setPassword(passwordEncoder.encode(password));
       user.setUsername(username);

       ///Create new Role
       Role roli = new Role();
       if(role.equals("admin")){
           roli.setId(1);
       }else{
           roli.setId(2);
       }
       user.setRole(roli);
       user.setPersonalId(personalID);

       ///Save User
       Integer nr = serviceIml.saveUser(user);

       ///If something went wrong return Not Found
       if(nr==null){
           return new ResponseEntity<String>("Something went wrong", HttpStatus.NOT_FOUND);
       }


       ///Everything is Ok
       return new ResponseEntity<String>("User saved successfully", HttpStatus.OK);
   }


    /**
     * METHOD THAT EDIT USER WHEN THE REQUEST IS MADE BY REST API
     */
    @PostMapping("/users/edit")
    public ResponseEntity<?> editUser(HttpServletRequest request){

        ///A map that will hold the errors
        Map<String, List<String>> errors = new HashMap<String, List<String>>();

        Integer id= Integer.parseInt(request.getParameter("id"));
        String firstName=request.getParameter("firstName");
        String lastName=request.getParameter("lastName");
        String gender=request.getParameter("gender");
        String birthday= request.getParameter("birthday");
        String personalID=request.getParameter("personalID");
        String email=request.getParameter("email");
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        String role=request.getParameter("role");



        ///Check if firstName is empty
        if(firstName.isEmpty()){
            errors.put("firstName", Arrays.asList("First Name cannot be null"));
        }

        ///Check if lastName is empty
        if(lastName.isEmpty()){
            errors.put("lastName", Arrays.asList("Last Name cannot be null"));
        }

        ///Check if gender is empty
        if(gender.equals("Select Gender")){
            errors.put("gender", Arrays.asList("Gender cannot be null"));
        }

        ///Check if birthday is empty
        if(birthday.isEmpty()){
            errors.put("birthday", Arrays.asList("Birthday cannot be null"));
        }

        ///Check if personalID is empty
        if(personalID.isEmpty()){
            errors.put("personalID", Arrays.asList("Personal ID cannot be null"));
        }

        ///Check if email is empty
        if(email.isEmpty()){
            errors.put("email", Arrays.asList("Email cannot be null"));
        }

        ///Check if username is empty
        if(username.isEmpty()){
            errors.put("username", Arrays.asList("Username cannot be null"));
        }

         ///Get the user
        UserDetails userToEdit=serviceIml.getUserById(id);

        ///Search in database a user with the username that user has written
        UserDetails user1 = serviceIml.getUserByUsername(username);


        ///If searched user is not null and user id it is not equal with the actual user that we want to edit
        ///That username is taken
        if(user1.getId()!=null && user1.getId()!=id){
            errors.put("username", Arrays.asList("Username already taken"));
        }

        ///Check if role is not null
        if(role.equals("Select Role")){
            errors.put("role", Arrays.asList("Role cannot be null"));
        }


        ///Check if map with errors is not empty
        if(!errors.isEmpty()){
            return new ResponseEntity<Map<String, List<String>>>(errors, HttpStatus.BAD_REQUEST);
        }

        ///Everything is validated

        ///Convert birthday to date
        java.sql.Date birthday1= java.sql.Date.valueOf(request.getParameter("birthday"));



        ///If admin has not writen a password in password field He/She dont want to change user password
        ///So let as it is, because the userToEdit is the user that we have searched in database
       if(!password.isEmpty()){
           ///Admin want to update password so we encode new password
           userToEdit.setPassword(passwordEncoder.encode(password));
        }

        userToEdit.setId(id);
        userToEdit.setFirstName(firstName);
        userToEdit.setLastName(lastName);
        userToEdit.setGender(gender.charAt(0));
        userToEdit.setIsEnabled(1);
        userToEdit.setBirthday(birthday1);
        userToEdit.setEmail(email);
        userToEdit.setUsername(username);

        ///Create new Role
        Role roli = new Role();
        if(role.equals("admin")){
            roli.setId(1);
        }else{
            roli.setId(2);
        }
        userToEdit.setRole(roli);
        userToEdit.setPersonalId(personalID);

        ///Update User
        Integer nr = serviceIml.updateUser(userToEdit);
        if(nr==null){
            ///If user did not updated
            return new ResponseEntity<String>("Something Went Wrong", HttpStatus.NOT_FOUND);
        }


        ///Everything it is Ok
        return new ResponseEntity<String>("User updated successfully", HttpStatus.OK);
    }


    /**
     * METHOD THAT DELETE AN USER WHEN REQUEST IS MADE BY REST API
     * @param id
     * @return
     */
    @GetMapping("/users/deleteUser/{id}")
    ResponseEntity<?> deleteUserById(@PathVariable Integer id) {


            Integer user = serviceIml.deleteUser(id);
            if(user==null){
                return new ResponseEntity<String>("User did not deleted", HttpStatus.NOT_FOUND);
            }

            return new ResponseEntity<String>("User deleted successfully", HttpStatus.OK);

    }

    /**
     * METHOD THAT ACTIVATE AN USER WHEN REQUEST IS MADE BY REST API
     */
    @GetMapping("/users/activateUser/{id}")
    ResponseEntity<?> activateUser(@PathVariable Integer id) {


        Integer user = serviceIml.activateUser(id);
        if(user==null){
            return new ResponseEntity<String>("User did not activated", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<String>("User activated successfully", HttpStatus.OK);

    }


    /******************************************
          METHOD THAT HANDLE REQUEST REST API
     *****************************************/

    @GetMapping("/requests/")
    List<Request> getAllAcceptedRequests() {


        return requestServiceIml.getAllAcceptedRequests();


    }

    @GetMapping("/requests/deleted")
    List<Request> getAllRejectedRequests() {

        return requestServiceIml.getAllDeletedRequests();


    }

    @GetMapping("/requests/waiting")
    List<Request> getAllWaitingRequests() {

        return requestServiceIml.getAllWaitingRequests();


    }



    @PostMapping("/request/changeStatus")
    ResponseEntity<?> changeTripStatus(HttpServletRequest request) {

        Integer status_id=Integer.parseInt(request.getParameter("status_id"));
        Integer trip_id=Integer.parseInt(request.getParameter("trip_id"));



        Integer nr = tripServiceIml.changeTripStatus(status_id, trip_id);

        if(nr==null){
            return new ResponseEntity<String>("Status did not changed", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<String>("Status changed successfully", HttpStatus.OK);

    }

    @GetMapping("/requests/user/{id}")
    List<Request> getAllRequestFromUserId(@PathVariable Integer id) {

        return requestServiceIml.getAllRequestsByUserId(id);


    }

    @PostMapping("/requests/searchByUsername")
    List<Request> getRequestsByUsername(HttpServletRequest request) {

        String name=request.getParameter("username");

        List<Request> nr = requestServiceIml.getAllRequestSearchedByUsername(name);

        return nr;

    }




     /***************************************
              HANDLE REQUEST FOR CHARTS
      ***************************************/


    @GetMapping("/charts/reasonsChart")
    List<chartsTest> getNumberOfReasons() {

        return tripServiceIml.getNumberOfReasonsOfTripsGroupedByReason();


    }

    @GetMapping("/charts/statusChart")
    List<barChartMapper> getNumberOfStatus() {

        return tripServiceIml.getNumberOfStatusOfTripsGroupedByStatus();


    }



   @GetMapping("getAllFlights")
   public List<Flight> getAllFlights(){
        return flightServiceIml.getAllFlights();
   }


/**************************
 *  TESTING THINGS
 **************************/

    /**
     * TESTING PURPOSES
     */
    @GetMapping("/test")
    List<?> getUserBy() {


        List<chartsTest> test = template.query("SELECT r.reason_title, COUNT(*) AS number FROM trip t INNER JOIN travellingreasons r ON" +
                " t.reason_id=r.reason_id WHERE t.status_id=3 " +
                " GROUP BY reason_title", new chartMapper());
       return test;


    }

    /**
     * For testing purposes while I was doing project (I will delete later)
     * @param user
     * @return
     */
    @PostMapping("/testUser")
    UserDetails add(@RequestBody UserDetails user){

        Session session = entityManager.unwrap(Session.class);
        session.save(user);
        return user;


    }









}
