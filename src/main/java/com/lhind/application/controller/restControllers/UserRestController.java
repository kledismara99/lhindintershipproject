package com.lhind.application.controller.restControllers;


import com.lhind.application.model.Flight;
import com.lhind.application.model.Request;
import com.lhind.application.model.Trip;
import com.lhind.application.model.UserDetails;
import com.lhind.application.repository.flightRepository.FlightRepositoryIml;
import com.lhind.application.service.adminService.AdminServiceIml;
import com.lhind.application.service.flightService.FlightServiceIml;
import com.lhind.application.service.requestService.RequestServiceIml;
import com.lhind.application.service.tripService.TripServiceIml;
import jdk.jfr.FlightRecorder;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


@RestController
@RequestMapping("/user/api/")
public class UserRestController {

    @Autowired
    TripServiceIml tripServiceIml;

    @Autowired
    FlightServiceIml flightServiceIml;

    @Autowired
    RequestServiceIml requestServiceIml;

    @Autowired
    AdminServiceIml adminServiceIml;


    /**
     * Method that handle request that add a trip in database
     * @param request
     * @return
     */
    @PostMapping("addTrip")
    ResponseEntity<?> addTrip(HttpServletRequest request) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        ///Map for trip reasons
        Map<String, Integer> tripReasons = new HashMap<>();
        tripReasons.put("Meeting", 1);
        tripReasons.put("Training", 2);
        tripReasons.put("Project", 3);
        tripReasons.put("Workshop", 4);
        tripReasons.put("Event", 5);
        tripReasons.put("Other", 6);

        ///Date formater to format date from input
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-DD");

        ///Date today
        LocalDate dateToday = LocalDate.now();

        ///Initialize departureDate and arrivalDate
        LocalDate departureDate = LocalDate.now();
        LocalDate arrivalDate = LocalDate.now();

        String tripReason = request.getParameter("tripReason");
        String tripDescription = request.getParameter("tripDescription");
        String fromPlace = request.getParameter("fromPlace");
        String toPlace = request.getParameter("toPlace");


        String departureDateS = request.getParameter("departureDate");
        String arrivalDateS = request.getParameter("arrivalDate");


        ///Map for errors
        Map<String, List<String>> errors = new HashMap<String, List<String>>();

        ///Check if trip reason is valid
        if (tripReasons.get(tripReason) == null || tripReason.isEmpty()) {
            errors.put("tripReason", Arrays.asList("Trip reason is not valid"));
        }

        ///Check if tripDescription is not empty
        if (tripDescription.isEmpty()) {
            errors.put("tripDescription", Arrays.asList("Trip description cannot be null"));
        }

        ///Check if fromPlace is not empty
        if (fromPlace.isEmpty()) {
            errors.put("fromPlace", Arrays.asList("From place cannot be null"));
        }

        ///Check if toPlace is not empty
        if (toPlace.isEmpty()) {
            errors.put("toPlace", Arrays.asList("To place cannot be null"));
        }

        ///Array list for departure date errors
        List<String> departureDateErrors = new ArrayList<>();

        ///Check if departure date is not empty
        if (departureDateS.isEmpty()) {
            departureDateErrors.add("Departure date cannot be null");
        } else {

            ///If is not empty initialize the departureDate with the date that is from input
            departureDate = LocalDate.parse(departureDateS);

            ///Compare if departureDate is before today date
            if (departureDate.compareTo(dateToday) < 0) {
                departureDateErrors.add("You should choose a future date");
            }
        }

        ///Array list for arrivalDate errors
        List<String> arrivalDateErrors = new ArrayList<>();

        ///Check if arrivalDate is empty
        if (arrivalDateS.isEmpty()) {
            arrivalDateErrors.add("Arrival date cannot be null");
        } else {

            ///It is not empty initialize the arrivalDate with the date that is from input
            arrivalDate = LocalDate.parse(arrivalDateS);

            ///If arrival date is before today date it is error
            if (arrivalDate.compareTo(dateToday) < 0) {
                arrivalDateErrors.add("You should choose a future date");
            } else
                ///Compare departureDate with arrivalDate, if arrivalDate is before departureDate it is error
                if (arrivalDate.compareTo(departureDate) < 0) {
                arrivalDateErrors.add("Arrival date should be greater than departure date");
            }
        }

        ///Check if departureDateErrors is not empty
        if (!departureDateErrors.isEmpty()) {
            errors.put("departureDate", departureDateErrors);
        }

        ///Check if arrivalDateErrors is not empty
        if (!arrivalDateErrors.isEmpty()) {
            errors.put("arrivalDate", arrivalDateErrors);
        }

        ///Check if map with errors is not empty
        if (!errors.isEmpty()) {
            System.out.println(tripReason + tripDescription + fromPlace + toPlace + departureDateS + arrivalDateS);
            return new ResponseEntity<Map<String, List<String>>>(errors, HttpStatus.BAD_REQUEST);
        }



        ///Everything it is Ok
        tripServiceIml.addNewTrip(tripDescription, fromPlace, toPlace, departureDate, arrivalDate, 1, tripReasons.get(tripReason), userId);
        return new ResponseEntity<String>("Trip saved successfully",HttpStatus.OK);
    }


    /**
     * Method that return all user trips by user Id
     * @return
     */
    @GetMapping("getTrips")
    public List<Trip> getAllTrips() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        return tripServiceIml.getTripsFromUserId(userId);
    }

    /**
     * Method that change trip status from user
     * @param request
     * @return
     */
    @PostMapping("changeTripStatus")
    public ResponseEntity<?> changeTripStatus(HttpServletRequest request) {

        Integer tripId = Integer.parseInt(request.getParameter("tripId"));

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        Trip trip = tripServiceIml.getTripById(tripId);

        ///Check if tripId is null because user might try a trip Id that does not exist, or he/she might try to
        ///delete another trip that he/she does not own it
        if (trip.getId() == null || trip.getUser().getId() != userId) {
            return new ResponseEntity<String>("This trip does not exist", HttpStatus.NOT_FOUND);
        }


        ///Change trip status to 2 (Send for approval)
        Integer nr = tripServiceIml.changeTripStatus(2, tripId);

        ///Create new request for admin
        Request request1=new Request();

        request1.setTrip(trip);
        request1.setCreatedDateTime(Timestamp.from(Instant.now()));
        request1.setIsSeenByAdmin(0);

        ///Save request
       Integer nr1 = requestServiceIml.saveRequest(request1);

       ///If something went wrong
        if (nr == null && nr1==null) {
            return new ResponseEntity<String>("Something went wrong", HttpStatus.NOT_FOUND);
        }

        ///Everything it is Ok
        return new ResponseEntity<String>("State changed successfully", HttpStatus.OK);
    }


    /**
     * Method that add a flight
     * @param request
     * @return
     */
    @PostMapping("addFlight")
    public ResponseEntity<?> addFlight(HttpServletRequest request) {

        ///Trip id from front end
        Integer tripId = Integer.parseInt(request.getParameter("tripId"));

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        ///Get trip from tripId
        Trip trip = tripServiceIml.getTripById(tripId);

        ///Check if tripId is null, maybe user have tried an id that does not exist, or maybe
        ///that trip does not have that user, or maybe trip is not approved
        if (trip.getId() == null || trip.getUser().getId() != userId || trip.getStatus().getId() != 3) {
            return new ResponseEntity<String>("This trip does not exist", HttpStatus.NOT_FOUND);
        }

        ///Date today
        LocalDate dateToday = LocalDate.now();

        ///User cannot add a flight if the departure date has been passed
        if(trip.getDepartureDate().compareTo(dateToday)<0){
            return new ResponseEntity<String>("You cannot add a flight becauese the departure date have been passed", HttpStatus.BAD_REQUEST);
        }
        ///Everything it is Ok

        ///Create new Flight
        Flight flight=new Flight();

        flight.setTrip(trip);
        flight.setIsDeleted(0);
        flight.setCreatedDateTime(Timestamp.from(Instant.now()));

        ///Save flight
        Integer nr =  (Integer) flightServiceIml.addFlight(flight);

        ///If flight does not saved
        if(nr==null){
            return new ResponseEntity<String>("Something went wrong", HttpStatus.NOT_FOUND);
        }

        ///Everything it is Ok
        return new ResponseEntity<String>("Trip saved succesfully", HttpStatus.OK);

    }


    /**
     * Method that delete an trip
     * @param request
     * @return
     */
    @PostMapping("deleteTrip")
    public ResponseEntity<?> deleteTrip(HttpServletRequest request){

        ///Trip id from front end
        Integer tripId = Integer.parseInt(request.getParameter("tripId"));

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        ///Get trip from tripId
        Trip trip = tripServiceIml.getTripById(tripId);

        ///Check if tripId is null, maybe user have tried an id that does not exist, or maybe
        ///that trip does not have that user, or maybe trip is not approved
        if (trip.getId() == null || trip.getUser().getId() != userId) {
            return new ResponseEntity<String>("This trip does not exist", HttpStatus.NOT_FOUND);
        }


        ///Delete flight
        Integer nr = (Integer) tripServiceIml.deleteTripById(tripId);

        ///If trip does not saved
        if(nr==null){
            return new ResponseEntity<String>("Something went wrong", HttpStatus.NOT_FOUND);
        }

        ///Everything it is Ok
        return new ResponseEntity<String>("Trip deleted succesfully", HttpStatus.OK);
    }


    /**
     * Method that activate a trip ----To be implemented in front
     * @param request
     * @return
     */
    @PostMapping("activateTrip")
    public ResponseEntity<?> activateTrip(HttpServletRequest request){

        ///Trip id from front end
        Integer tripId = Integer.parseInt(request.getParameter("tripId"));

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        ///Get trip from tripId
        Trip trip = tripServiceIml.getTripById(tripId);

        ///Check if tripId is null, maybe user have tried an id that does not exist, or maybe
        ///that trip does not have that user, or maybe trip is not approved
        if (trip.getId() == null || trip.getUser().getId() != userId) {
            return new ResponseEntity<String>("This trip does not exist", HttpStatus.NOT_FOUND);
        }

        LocalDate dateToday = LocalDate.now();

        ///User cannot activate an trip if it`s departure date is less than date today because it does not have sens
        if(trip.getDepartureDate().compareTo(dateToday)<0){
            return new ResponseEntity<String>("You cannot activate this trip because the departure date have past", HttpStatus.BAD_REQUEST);
        }


        ///Activate trip
        Integer nr = (Integer) tripServiceIml.activateTrip(tripId);

        ///If trip does not activated
        if(nr==null){
            return new ResponseEntity<String>("Something went wrong", HttpStatus.NOT_FOUND);
        }

        ///Everything it is Ok
        return new ResponseEntity<String>("Trip activatet succesfully", HttpStatus.OK);
    }

    /**
     * Method that return a trip by it is Id
     * @param request
     * @return
     */
    @PostMapping("getTripById")
    public ResponseEntity<?> getTripById(HttpServletRequest request){

        ///Trip id from front end
        Integer tripId = Integer.parseInt(request.getParameter("tripId"));

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        ///Check if tripId is null, maybe user have tried an id that does not exist, or maybe
        ///that trip does not have that user, or maybe trip is not approved
        Trip trip = tripServiceIml.getTripById(tripId);
        if (trip.getId() == null || trip.getUser().getId() != userId) {
            return new ResponseEntity<String>("This trip does not exist", HttpStatus.NOT_FOUND);
        }


        ///Everything it is Ok
        return new ResponseEntity<Trip>(trip, HttpStatus.OK);
    }


    /**
     * Method that return trip from user id
     * @return
     */
    @GetMapping("getTripFromUserId")
    public ResponseEntity<?> getTripsFromUserId(){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        if(userId==null){
            return new ResponseEntity<String>("You dont have permision", HttpStatus.UNAUTHORIZED);
        }

        List<Flight> flights=flightServiceIml.getFlightsByUserId(userId);


        ///Everything it is Ok
        return new ResponseEntity<List<Flight>>(flights, HttpStatus.OK);
    }


    /**
     * Method that delete a flight
     * @param request
     * @return
     */
    @PostMapping("deleteFlight")
    public ResponseEntity<?> deleteFlight(HttpServletRequest request){

        ///Flight id from front end
        Integer flightId = Integer.parseInt(request.getParameter("flightId"));

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        Flight flight = flightServiceIml.getFlightById(flightId);

        //Check if flightId is null, maybe user have tried an id that does not exist, or maybe
        ///that flight does not have that user
        if (flight.getId() == null || flight.getTrip().getUser().getId() != userId) {
            return new ResponseEntity<String>("This flight does not exist", HttpStatus.NOT_FOUND);
        }

        ///Delete flight
        Integer nr=flightServiceIml.deleteFlight(flightId);

        ///Check if flight is not deleted
        if(nr==null){
            return new ResponseEntity<String>("Something went wrong", HttpStatus.NOT_FOUND);
        }

        ///Everything it is Ok
        return new ResponseEntity<String>("Flight deleted succesfully", HttpStatus.OK);
    }


    /**
     * Testing purposes
     * @return
     */
    @GetMapping("test")
    public BigInteger test(){
       return flightServiceIml.getNumberOfAllFlightsByUserId(1);
    }

}