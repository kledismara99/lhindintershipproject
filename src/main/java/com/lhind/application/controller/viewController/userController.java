package com.lhind.application.controller.viewController;

import com.lhind.application.model.Flight;
import com.lhind.application.model.Trip;
import com.lhind.application.model.UserDetails;
import com.lhind.application.service.adminService.AdminServiceIml;
import com.lhind.application.service.flightService.FlightServiceIml;
import com.lhind.application.service.tripService.TripServiceIml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class userController {

    Logger logger= LoggerFactory.getLogger(userController.class);

    @Autowired
    AdminServiceIml adminServiceIml;

    @Autowired
    TripServiceIml tripServiceIml;

    @Autowired
    FlightServiceIml flightServiceIml;




    /**
     * Method that return view of add trip
     * @param model
     * @return
     */
    @GetMapping("/user/view/addTrip")
    public String addTrip(Model model){

        logger.info("User has accessed View to Add a Trip");
        return "/User/Trips/addTrip";

    }

    /**
     * Method that return view of trips
     * @param model
     * @return
     */
    @GetMapping("/user/view/trips")
    public String getAllTrips(Model model){

        logger.info("User has accessed View to show All Trips");
        return "/User/Trips/showAllTrips";

    }


    /**
     * Method that return view of a trip
     * @param model
     * @param tripId
     * @return
     */
    @GetMapping("/user/view/trip/{tripId}")
    public String getTripView(Model model, @PathVariable Integer tripId){

        logger.info("User has accessed View to show a Trip");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        UserDetails authenticatedUser=adminServiceIml.getUserByUsername(auth.getName());
        Integer userId=authenticatedUser.getId();

        Trip trip = tripServiceIml.getTripById(tripId);

        ///Check if trip exist and if user own it
        if (trip.getId() == null || trip.getUser().getId() != userId) {
            return "/error.html";
        }

        ///Pass result in view
        model.addAttribute("trip", trip);
        return "/User/Trips/showTrip";

    }

    /**
     * Method that return view of fliggts
     * @param model
     * @return
     */
    @GetMapping("/user/view/flights")
    public String getAllFlights(Model model){

        logger.info("User has accessed View to show flights");
        return "/User/Flights/showFlights";

    }

    /**
     * Method that return flights from a trip
     * @param model
     * @param tripId
     * @return
     */
    @GetMapping("/user/view/flightsFromTrip/{tripId}")
    public String getFlightsFromTripId(Model model, @PathVariable Integer tripId){

        logger.info("User has accessed View to show flights from a trip");
        List<Flight> flights=flightServiceIml.getAllFlightsByTripId(tripId);
        model.addAttribute("flights", flights);
        return "/User/Flights/showFlightFromTrip";

    }

}
