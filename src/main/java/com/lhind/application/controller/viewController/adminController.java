package com.lhind.application.controller.viewController;

import com.lhind.application.controller.restControllers.AdminRestController;
import com.lhind.application.model.Request;
import com.lhind.application.model.UserDetails;
import com.lhind.application.service.adminService.AdminServiceIml;
import com.lhind.application.service.flightService.FlightServiceIml;
import com.lhind.application.service.requestService.RequestServiceIml;
import com.lhind.application.service.tripService.TripServiceIml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.math.BigInteger;
import java.util.List;

@Controller
public class adminController {

    Logger logger= LoggerFactory.getLogger(adminController.class);

    @Autowired
    AdminServiceIml adminServiceIml;

    @Autowired
    TripServiceIml tripServiceIml;

    @Autowired
    RequestServiceIml requestServiceIml;

    @Autowired
    FlightServiceIml flightServiceIml;

    /*

            METHOD THAT RETURNS ALL ADMIN USERS ACTIONS

     */


    /**
     * Method that return view of users
     * @param model
     * @return
     */
    @GetMapping("/admin/users")
    public String getAllUsers(Model model){

        logger.info("Admin has accessed Users View");
        List<UserDetails> users=adminServiceIml.getAllUsers();
        model.addAttribute("users", users);
        return "/Admin/User/allUsers";
    }


    /**
     * Method that return view of add user
     * @param model
     * @return
     */
    @GetMapping("/admin/users/add")
    public String addNewUser(Model model){
        logger.info("Admin has accessed View to Add an User");
        List<UserDetails> users=adminServiceIml.getAllUsers();
        model.addAttribute("users", users);
        return "/Admin/User/addUser";
    }

    /**
     * Method that return view of edit user
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/admin/users/edit/{id}")
    public String editUser(Model model, @PathVariable Integer id){

        logger.info("Admin has accessed View to Edit an User");
        UserDetails users=adminServiceIml.getUserById(id);
        model.addAttribute("user", users);
        return "/Admin/User/editUser";
    }

    /**
     * Method that return view of deleted users
     * @param model
     * @return
     */
    @GetMapping("/admin/users/deleted")
    public String deletetUsers(Model model){

        logger.info("Admin has accessed View to see all deletet users");
        List<UserDetails> users=adminServiceIml.getAllDeletedUsers();
        model.addAttribute("users", users);
        return "/Admin/User/deletedUsers";
    }

    /*

            METHOD THAT RETURNS ALL ADMIN REQUESTS ACTIONS

     */

    /**
     * Method that return view of requests
     * @param model
     * @return
     */
    @GetMapping("/admin/view/requests")
    public String getAllAcceptedRequests(Model model){

        logger.info("Admin has accessed View of all accepted requests");
        return "/Admin/Request/acceptedRequests";
    }

    /**
     * Method that return view of deleted requests
     * @param model
     * @return
     */
    @GetMapping("/admin/view/requests/deleted")
    public String getAllRejectedRequests(Model model){

        logger.info("Admin has accessed View of all deleted requests");
        return "/Admin/Request/rejectedRequests";
    }

    /**
     * Method that return view of waiting requests
     * @param model
     * @return
     */
    @GetMapping("/admin/view/requests/waiting")
    public String getAllWaitingRequests(Model model){

        logger.info("Admin has accessed View of all waiting requests");
        return "/Admin/Request/waitingRequests";
    }

    /**
     * Method that return view of a request
     * @param model
     * @param id
     * @return
     */
    @GetMapping("/admin/view/requests/{id}")
    public String getARequests(Model model, @PathVariable Integer id){


        logger.info("Admin has accessed View of a request");
        Request rq = requestServiceIml.getARequest(id);
        ///If admin see for the first time request, update is_seen_by_admin
        if(rq.getIsSeenByAdmin()!=1){
            Integer x = requestServiceIml.updateIsSeenByAdmin(id);
        }
        Request request = requestServiceIml.getARequest(id);
        model.addAttribute("request", request);
        return "/Admin/Request/showRequest";
    }


    /**
     * Method that return view of flights
     * @param model
     * @return
     */
    @GetMapping("/admin/view/flights")
    public String getAllFlights(Model model){

        logger.info("Admin has accessed View of all flights");
        return "/Admin/Flights/showAllFlights";
    }


}
