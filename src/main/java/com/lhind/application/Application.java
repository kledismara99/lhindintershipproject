package com.lhind.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class Application {

public static void main(String [] args){

    SpringApplication.run(Application.class, args);

    /*Course java=new Course(1, "JAVA", "JAVA SE8");
    Course spring=new Course(2, "SPRING", "Spring");

    Registration registration=new Registration(1, "Kurse JAVA");
    Set<Course> kurset = new HashSet<>();
    kurset.add(java);
    kurset.add(spring);
    registration.setCourseSet(kurset);

    reg.save(registration);*/



}


}
