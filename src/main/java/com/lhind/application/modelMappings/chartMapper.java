package com.lhind.application.modelMappings;

import com.lhind.application.model.UserDetails;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class chartMapper implements RowMapper<chartsTest> {
    @Override
    public chartsTest mapRow(ResultSet resultSet, int i) throws SQLException {
        chartsTest test=new chartsTest();
        test.setRole_title(resultSet.getString("reason_title"));
        test.setNumber(resultSet.getInt("number"));
        return test;
    }
}
