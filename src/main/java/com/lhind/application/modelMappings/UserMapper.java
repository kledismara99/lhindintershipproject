package com.lhind.application.modelMappings;

import com.lhind.application.model.Role;
import com.lhind.application.model.Trip;
import com.lhind.application.model.UserDetails;
import org.springframework.jdbc.core.RowMapper;


import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<UserDetails> {


    public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
        Role role = new Role();
        if(rs.getInt("role_id")==1){
            role.setId(1);
            role.setRoleTitle("USER");
        } else {
            role.setId(2);
            role.setRoleTitle("ADMIN");
        }



        UserDetails user = new UserDetails();
                user.setId(rs.getInt("user_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setBirthday(rs.getDate("birthday"));
                user.setGender(rs.getString("gender").charAt(0));
                user.setPersonalId(rs.getString("personal_id"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setUsername(rs.getString("username"));
                user.setIsEnabled(rs.getInt("is_user_enabled"));
                user.setCreated_date_time(rs.getTimestamp("created_date_time"));
                user.setRole(role);


        return user;
    }


}
