package com.lhind.application.modelMappings;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class barChartMapper implements RowMapper<barChartMapper> {

    String statusTitle;
    Integer statusNumber;


    public barChartMapper() {
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public Integer getStatusNumber() {
        return statusNumber;
    }

    public void setStatusNumber(Integer statusNumber) {
        this.statusNumber = statusNumber;
    }



    @Override
    public barChartMapper mapRow(ResultSet resultSet, int i) throws SQLException {
        barChartMapper test=new barChartMapper();
        test.setStatusTitle(resultSet.getString("status_title"));
        test.setStatusNumber(resultSet.getInt("number"));
        return test;
    }
}
