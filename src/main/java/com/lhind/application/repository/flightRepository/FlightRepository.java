package com.lhind.application.repository.flightRepository;


import com.lhind.application.model.Flight;

import java.math.BigInteger;
import java.util.List;

public interface FlightRepository {

    Integer addFlight(Flight flight);
    List<Flight> getFlightsByUserId(Integer id);
    List<Flight> getAllFlights();
    Flight getFlightById(Integer id);
    List<Flight> getAllFlightsByTripId(Integer id);
    Integer deleteFlight(Integer flightId);
    BigInteger getNumberOfAllFlights();
    BigInteger getNumberOfAllFlightsByUserId(Integer userId);

}
