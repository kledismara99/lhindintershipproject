package com.lhind.application.repository.flightRepository;



import com.lhind.application.model.Flight;
import com.lhind.application.model.Request;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.List;

@Repository
public class FlightRepositoryIml implements FlightRepository{

    private static final String DELETE_FLIGHT_BY_ID ="UPDATE flights SET is_deleted = 1  WHERE flight_id = ?";

    @Autowired
    EntityManager entityManager;

    @Autowired
    JdbcTemplate jdbcTemplate;


    @Override
    public Integer addFlight(Flight flight) {
        Session session = entityManager.unwrap(Session.class);
        Integer nr = (Integer) session.save(flight);
        return nr;
    }

    @Override
    public List<Flight> getFlightsByUserId(Integer id) {
        Session session = entityManager.unwrap(Session.class);
        Query<Flight> query= session.createQuery("from Flight WHERE trip.user.id="+id+" AND isDeleted=0 AND trip.isDeleted=0", Flight.class);
        List<Flight> flights = query.getResultList();
        return flights;
    }

    @Override
    public List<Flight> getAllFlights() {
        Session session = entityManager.unwrap(Session.class);
        Query<Flight> query= session.createQuery("from Flight WHERE isDeleted=0 AND trip.isDeleted=0", Flight.class);
        List<Flight> flights = query.getResultList();
        return flights;
    }

    @Override
    public Flight getFlightById(Integer id) {
        Session session = entityManager.unwrap(Session.class);
        Query<Flight> query= session.createQuery("from Flight WHERE is_deleted=0 AND flight_id="+id, Flight.class);
        Flight flight = query.getSingleResult();
        return flight;

    }

    @Override
    public List<Flight> getAllFlightsByTripId(Integer id) {

        Session session = entityManager.unwrap(Session.class);
        Query<Flight> query= session.createQuery("from Flight WHERE trip.id="+id+" AND isDeleted=0 AND trip.isDeleted=0", Flight.class);
        List<Flight> flights = query.getResultList();
        return flights;
    }

    @Override
    public Integer deleteFlight(Integer flightId) {
        Integer nr = (Integer) jdbcTemplate.update(DELETE_FLIGHT_BY_ID, new Object[]{flightId});
        return nr;
    }

    @Override
    public BigInteger getNumberOfAllFlights() {

        Session session = entityManager.unwrap(Session.class);
        Query query= session.createSQLQuery("SELECT COUNT(*) from flights f INNER JOIN trip t ON f.trip_id=t.trip_id " +
                "WHERE f.is_deleted=0 AND t.is_deleted=0");
        BigInteger nr =  (BigInteger) query.getSingleResult();
        return nr;
    }

    @Override
    public BigInteger getNumberOfAllFlightsByUserId(Integer userId) {
        Session session = entityManager.unwrap(Session.class);
        Query query= session.createSQLQuery("SELECT COUNT(*) from flights f INNER JOIN trip t ON f.trip_id=t.trip_id " +
                "WHERE f.is_deleted=0 AND t.is_deleted=0 AND t.user_id="+userId);
        BigInteger nr =  (BigInteger) query.getSingleResult();
        return nr;
    }
}
