package com.lhind.application.repository.statusRepository;


import com.lhind.application.model.Status;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.provider.HibernateUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.beans.Statement;
import java.io.Serializable;
import java.util.List;

@Repository
public class StatusRepositoryIml implements StatusRepository{

    /**
     *
     *       SQL QUERIES
     *
     */
    private static final String UPDATE_STATUS="UPDATE status SET status_title = ? WHERE status_id = ?";


    @Autowired
    EntityManager entityManager;

    @Autowired
    JdbcTemplate template;

    @Override
    public Status addNewStatus(Status status) {

        Session session = entityManager.unwrap(Session.class);
        session.save(status);
        session.close();
        return status;
    }

    @Override
    public void updateStatus(Status status) {
        template.update(UPDATE_STATUS, new Object[]{status.getTitle(), status.getId()});
    }

    @Override
    public void deleteStatus(Status status) {
        Session session = entityManager.unwrap(Session.class);
        session.update(status);
        session.close();
    }

    @Override
    public List<Status> getAllStatus() {
        Session session = entityManager.unwrap(Session.class);
        Query<Status> query= session.createQuery("from Status", Status.class);
        List<Status> statusList = query.getResultList();
        return statusList;
    }
}
