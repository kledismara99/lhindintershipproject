package com.lhind.application.repository.statusRepository;


import com.lhind.application.model.Status;

import java.util.List;

public interface StatusRepository {
    Status addNewStatus(Status status);
    void updateStatus(Status status);
    void deleteStatus(Status status);
    List<Status> getAllStatus();
}
