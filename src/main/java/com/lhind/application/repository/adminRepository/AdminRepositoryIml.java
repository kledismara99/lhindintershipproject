package com.lhind.application.repository.adminRepository;

import com.lhind.application.model.UserDetails;


import org.apache.catalina.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.lhind.application.modelMappings.UserMapper;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.math.BigInteger;
import java.util.List;

@Repository
public class AdminRepositoryIml implements AdminRepository {

    /**
     *
     *      ALL SQL QUERIES THAT WILL BE USED IN ADMIN REPOSITORY TO GET DATA FROM
     *                                   DATABASE
     */

    private final static String GET_ALL_USERS="SELECT * FROM userdetails";
    private final static String GET_ALL_DELETED_USERS="SELECT * FROM userdetails WHERE is_user_enabled=0";
    private final static String GET_ONE_USER="SELECT * FROM userdetails WHERE user_id = ?";
    private final static String INSERT_NEW_USER="INSERT INTO userdetails(" +
            "first_name, " +
            "last_name," +
            "birthday" +
            "gender," +
            "personal_id," +
            "email," +
            "password," +
            "created_date_time," +
            "is_user_enabled" +
            "role_id) " +
            "VALUES(?,?,?,?,?,?,?,?,?,?)";
    private final static String UPDATE_USER="UPDATE userdetails " +
            "SET " +
            "first_name = ? , " +
            "last_name = ? ," +
            "birthday = ? ," +
            "gender = ? ," +
            "personal_id = ? ," +
            "email = ? ," +
            "password = ? ," +
            "role_id = ? ," +
            "username = ?" +
            "WHERE user_id = ? ;";
    private final static String SEARCH_USER="SELECT * FROM userdetails WHERE first_name LIKE \"%?%\"";
    private final static String DELETE_AN_USER="UPDATE userdetails SET is_user_enabled=0 WHERE user_id= ?";
    private static final String ACTIVATE_AN_USER ="UPDATE userdetails SET is_user_enabled=1 WHERE user_id= ?";


    @Autowired
    EntityManager entityManager;
    @Autowired
    JdbcTemplate template;

    /**
     * Method that search a User by it`s name and return a list of users
     * @param name
     * @return users
     */

    @Override
    public List<UserDetails> searchUsers(String name) {
        Session session = entityManager.unwrap(Session.class);
        Query<UserDetails> query= session.createQuery("from UserDetails WHERE first_name LIKE '%"+name+"%'", UserDetails.class);
        List<UserDetails> users = query.getResultList();
        return users;
    }

    /**
     * Method that get all users from database and mapes their data with UserMapper
     * @return
     */
    @Override
    public List<UserDetails> getAllUsers() {
        ///List<UserDetails> users = template.query(GET_ALL_USERS, new UserMapper());
        Session session = entityManager.unwrap(Session.class);
        Query<UserDetails> query= session.createQuery("from UserDetails", UserDetails.class);
        List<UserDetails> users = query.getResultList();
        return users;
    }

    /**
     * Method that return all deleted users. It impelment GET_ALL_DELETED_USERS SQL Query
     * @return
     */
    @Override
    public List<UserDetails> getAllDeletedUsers() {

        Session session = entityManager.unwrap(Session.class);
        Query<UserDetails> query= session.createQuery("from UserDetails WHERE isEnabled=0", UserDetails.class);
        List<UserDetails> users = query.getResultList();
        return users;
    }

    /**
     * Method that return an user by Id
     * @param id
     * @return
     */
    @Override
    public UserDetails getUserById(Integer id) {

        UserDetails user = template.queryForObject(GET_ONE_USER, new Object[]{id}, new UserMapper());
        return user;
    }

    /**
     * Method that save a user
     * @param user
     * @return
     */
    @Override
    public Integer saveUser(UserDetails user) {

        Session session = entityManager.unwrap(Session.class);
        Integer nr = (Integer) session.save(user);
        return nr;
    }

    /**
     * Method that update an user, first it check if user exist in database
     * if not, it throws an error
     * @param user
     * @return
     */
    @Override
    public Integer updateUser(UserDetails user){

        Integer nr = (Integer) template.update(UPDATE_USER, new Object[]{
                user.getFirstName(),
                user.getLastName(),
                user.getBirthday(),
                user.getGender()+" ",
                user.getPersonalId(),
                user.getEmail(),
                user.getPassword(),
                user.getRole().getId(),
                user.getUsername(),
                user.getId()});

        return nr;

    }

    /**
     * Method that delete an user
     * @param id
     */

    @Override
    public Integer deleteUser(Integer id) {
        Integer nr = (Integer) template.update(DELETE_AN_USER, new Object[]{id});
        return nr;
    }

    @Override
    public Integer activateUser(Integer id) {
        Integer nr = (Integer) template.update(ACTIVATE_AN_USER, new Object[]{id});
        return nr;
    }


    public UserDetails getUserByUsername(String username){
        Session session = entityManager.unwrap(Session.class);
        Query<UserDetails> query= session.createQuery("from UserDetails WHERE username='"+username+"'", UserDetails.class);
        try {
            UserDetails user = (UserDetails) query.getSingleResult();
            return user;
        } catch (NoResultException e){
            return new UserDetails();
        }
    }

    @Override
    public BigInteger getNumberOfAllUsers() {
        Session session = entityManager.unwrap(Session.class);
        Query query= session.createSQLQuery("SELECT COUNT(*) from userdetails WHERE is_user_enabled=1");
        BigInteger nr = (BigInteger) query.getSingleResult();
        return nr;
    }

}
