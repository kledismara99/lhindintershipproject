package com.lhind.application.repository.adminRepository;

import com.lhind.application.Exceptions.UserNotFoundException;
import com.lhind.application.model.UserDetails;
import com.lhind.application.modelMappings.chartsTest;
import org.apache.catalina.User;

import java.math.BigInteger;
import java.util.List;

public interface AdminRepository {

    List<UserDetails> searchUsers(String name);
    List<UserDetails> getAllUsers();
    List<UserDetails> getAllDeletedUsers();
    UserDetails getUserById(Integer id);
    Integer saveUser(UserDetails user);
    Integer updateUser(UserDetails user);
    Integer deleteUser(Integer id);
    Integer activateUser(Integer id);
    UserDetails getUserByUsername(String name);
    BigInteger getNumberOfAllUsers();


}
