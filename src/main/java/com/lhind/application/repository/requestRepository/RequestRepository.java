package com.lhind.application.repository.requestRepository;

import com.lhind.application.model.Request;
import com.lhind.application.model.Trip;

import java.math.BigInteger;
import java.util.List;

public interface RequestRepository {


    Integer saveRequest(Request request);
    List<Request> getAllRequests();
    Request getARequest(Integer id);
    Integer updateIsSeenByAdmin(Integer id);
    Integer acceptRequest(Integer id);
    String deleteRequest(Integer id);
    List<Request> getAllAcceptedRequests();
    List<Request> getAllWaitingRequests();
    List<Request> getAllDeletedRequests();
    BigInteger getAllNotSeenRequests();
    List<Request> getAllRequestsByUserId(Integer id);
    List<Request> getAllRequestSearchedByUsername(String name);

}
