package com.lhind.application.repository.requestRepository;

import com.lhind.application.model.Request;
import com.lhind.application.model.Trip;
import com.lhind.application.model.UserDetails;
import com.lhind.application.repository.tripRepository.TripRepositoryIml;
import com.lhind.application.service.tripService.TripServiceIml;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.List;

@Repository
public class RequestRepositoryIml implements RequestRepository{



    @Autowired
    EntityManager entityManager;
    @Autowired
    JdbcTemplate template;

    @Autowired
    TripServiceIml tripServiceIml;


    private static final String CHANGE_IS_SEEN_BY_ADMIN="UPDATE Request SET is_seen_by_admin=1 WHERE request_id = ?";


    @Override
    public Integer saveRequest(Request request) {
        Session session = entityManager.unwrap(Session.class);
        Integer nr= (Integer) session.save(request);
        return nr;
    }

    @Override
    public List<Request> getAllRequests() {
        Session session = entityManager.unwrap(Session.class);
        Query<Request> query= session.createQuery("from Request", Request.class);
        List<Request> requests = query.getResultList();
        return requests;
    }

    @Override
    public Request getARequest(Integer id) {
        Session session = entityManager.unwrap(Session.class);
        Query<Request> query= session.createQuery("from Request WHERE request_id="+id, Request.class);
        Request request = query.getSingleResult();
        return request;
    }

    @Override
    public Integer updateIsSeenByAdmin(Integer id) {
        Integer nr = (Integer) template.update(CHANGE_IS_SEEN_BY_ADMIN, new Object[]{id});
        return nr;
    }

    @Override
    public Integer acceptRequest(Integer id) {
        Request request=getARequest(id);
        Integer nr = tripServiceIml.changeTripStatus(3, request.getTrip().getId());
        return nr;
    }

    @Override
    public String deleteRequest(Integer id) {
       return null;
    }

    @Override
    public List<Request> getAllAcceptedRequests() {
        Session session = entityManager.unwrap(Session.class);
        Query<Request> query= session.createQuery("FROM Request WHERE trip.status.id=3 AND trip.isDeleted=0", Request.class);
        List<Request> requests = query.getResultList();
        return requests;
    }

    @Override
    public List<Request> getAllWaitingRequests() {
        Session session = entityManager.unwrap(Session.class);
        Query<Request> query= session.createQuery("FROM Request WHERE trip.status.id=2 AND trip.isDeleted=0", Request.class);
        List<Request> requests = query.getResultList();
        return requests;
    }

    @Override
    public List<Request> getAllDeletedRequests() {

        Session session = entityManager.unwrap(Session.class);
        Query<Request> query= session.createQuery("FROM Request WHERE trip.status.id=4", Request.class);
        List<Request> requests = query.getResultList();
        return requests;
    }

    @Override
    public BigInteger getAllNotSeenRequests() {
        Session session = entityManager.unwrap(Session.class);
        Query query= session.createSQLQuery("SELECT COUNT(*) FROM request WHERE is_seen_by_admin=0");
        BigInteger nr = (BigInteger) query.getSingleResult();
        return nr;
    }

    @Override
    public List<Request> getAllRequestsByUserId(Integer id) {
        Session session = entityManager.unwrap(Session.class);
        Query<Request> query= session.createQuery("FROM Request WHERE trip.user.id="+id, Request.class);
        List<Request> requests = query.getResultList();
        return requests;
    }

    @Override
    public List<Request> getAllRequestSearchedByUsername(String name) {
        Session session = entityManager.unwrap(Session.class);
        Query<Request> query= session.createQuery("FROM Request WHERE trip.user.firstName LIKE '%"+name+"%'", Request.class);
        List<Request> requests = query.getResultList();
        return requests;
    }
}
