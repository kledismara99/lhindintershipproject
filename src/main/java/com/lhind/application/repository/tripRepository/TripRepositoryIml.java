package com.lhind.application.repository.tripRepository;

import com.lhind.application.model.Request;
import com.lhind.application.model.Trip;
import com.lhind.application.modelMappings.barChartMapper;
import com.lhind.application.modelMappings.chartMapper;
import com.lhind.application.modelMappings.chartsTest;
import org.hibernate.JDBCException;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public class TripRepositoryIml implements TripRepository{


    private final static String CREATE_TRIP="INSERT INTO trip " +
            "(" +
            "trip_description ," +
            "from_place ," +
            "to_place ," +
            "departure_date ," +
            "arrival_date , " +
            "status_id ," +
            "reason_id ," +
            "user_id ) " +
            "VALUES(?,?,?,?,?,?,?,?);";

    private final static String UPDATE_TRIP="UPDATE trip " +
            "SET" +
            "trip_description= ? ," +
            "from_place= ? ," +
            "to_place= ? ," +
            "departure_date= ? ," +
            "arrival_date= ? , " +
            "status_id= ? ," +
            "user_id= ? " +
            "WHERE trip_id= ? ;";

    private static final String CHANGE_TRIP_STATUS ="UPDATE trip SET status_id = ?  WHERE trip_id = ?";

    private static final String ACTIVATE_TRIP="UPDATE trip SET is_deleted=0 WHERE trip_id = ?";
    private static final String DELETE_TRIP="UPDATE trip SET is_deleted=1 WHERE trip_id = ?";
    @Autowired
    JdbcTemplate template;

    @Autowired
    EntityManager entityManager;

    @Override
    public Integer addNewTrip(String trip_description,
                           String from_place,
                           String to_place,
                           LocalDate departure_date,
                           LocalDate arrival_date,
                           Integer status_id,
                           Integer reason_id,
                           Integer user_id) {

            Integer nr = (Integer) template.update(CREATE_TRIP, new Object[]{
                    trip_description,
                    from_place,
                    to_place,
                    departure_date,
                    arrival_date,
                    1,
                    reason_id,
                    user_id
            });

        return nr;
    }


    @Override
    public Trip getTripById(Integer id) {
        Session session = entityManager.unwrap(Session.class);
        try{
            Query<Trip> query= session.createQuery("from Trip WHERE trip_id="+id+" AND isDeleted=0", Trip.class);
            Trip lista = query.getSingleResult();
            return lista;
        } catch (NoResultException e){
            return new Trip();
        }

    }

    @Override
    public List<Trip> getTripsFromUserId(Integer id) {
        Session session = entityManager.unwrap(Session.class);
        Query<Trip> query= session.createQuery("from Trip WHERE user_id="+id+" AND isDeleted=0", Trip.class);
        List<Trip> lista = query.getResultList();
        return lista;
    }

    @Override
    public List<Trip> getAllTrips() {
        Session session = entityManager.unwrap(Session.class);
        Query<Trip> query= session.createQuery("from Trip", Trip.class);
        List<Trip> lista = query.getResultList();
        return lista;
    }

    @Override
    public Integer updateTripById(String trip_description,
                                 String from_place,
                                 String to_place,
                                 Date departure_date,
                                 Date arrival_date,
                                 Integer status_id,
                                 Integer user_id,
                                 Integer id) {


            Integer nr = (Integer) template.update(UPDATE_TRIP, new Object[]{
                    trip_description,
                    from_place,
                    to_place,
                    departure_date,
                    arrival_date,
                    status_id,
                    user_id,
                    id
            });

            return nr;

    }

    @Override
    public Integer activateTrip(Integer id) {
        Integer nr= (Integer) template.update(ACTIVATE_TRIP, new Object[]{id});

        return nr;
    }

    @Override
    public Integer deleteTripById(Integer id) {
        try{
            Integer nr= (Integer) template.update(DELETE_TRIP, new Object[]{id});
            return nr;
        }catch (NoResultException e){
            return null;
        }


    }

    @Override
    public Integer changeTripStatus(Integer status_id, Integer trip_id) {
        Integer nr = (Integer) template.update(CHANGE_TRIP_STATUS, new Object[]{status_id, trip_id});
        return nr;
    }

    @Override
    public BigInteger getNumberOfTrips() {
        Session session = entityManager.unwrap(Session.class);
        Query query= session.createSQLQuery("SELECT COUNT(*) from trip WHERE status_id=3");
        BigInteger nr = (BigInteger) query.getSingleResult();
        return nr;
    }

    @Override
    public BigInteger getNumberOfWaitingTrips() {
        Session session = entityManager.unwrap(Session.class);
        Query query= session.createSQLQuery("SELECT COUNT(*) from trip WHERE status_id=2");
        BigInteger nr = (BigInteger) query.getSingleResult();
        return nr;
    }

    @Override
    public BigInteger getNumberOfTripsByUserId(Integer id) {
        Session session = entityManager.unwrap(Session.class);
        Query query= session.createSQLQuery("SELECT COUNT(*) from trip WHERE is_deleted=0 AND status_id=3 AND user_id="+id);
        BigInteger nr =  (BigInteger) query.getSingleResult();
        return nr;
    }

    @Override
    public BigInteger getNumberOfWaitingTripsByUserId(Integer id) {
        Session session = entityManager.unwrap(Session.class);
        Query query= session.createSQLQuery("SELECT COUNT(*) from trip WHERE is_deleted=0 AND status_id=2 AND user_id="+id);
        BigInteger nr =  (BigInteger) query.getSingleResult();
        return nr;
    }

    @Override
    public BigInteger getNumberOfCreatedTripsByUserId(Integer id) {
        Session session = entityManager.unwrap(Session.class);
        Query query= session.createSQLQuery("SELECT COUNT(*) from trip WHERE is_deleted=0 AND status_id=1 AND user_id="+id);
        BigInteger nr =  (BigInteger) query.getSingleResult();
        return nr;
    }

    @Override
    public List<chartsTest> getNumberOfReasonsOfTripsGroupedByReason() {

        List<chartsTest> chartsTestList = template.query("SELECT r.reason_title, COUNT(*) AS number FROM trip t INNER JOIN travellingreasons r ON" +
                " t.reason_id=r.reason_id WHERE t.status_id=3 " +
                " GROUP BY r.reason_title", new chartMapper());
        return chartsTestList;
    }

    @Override
    public List<barChartMapper> getNumberOfStatusOfTripsGroupedByStatus() {
        List<barChartMapper> chartsTestList = template.query("SELECT s.status_title, COUNT(*) AS number FROM trip t INNER JOIN status s ON" +
                " t.status_id=s.status_id" +
                " GROUP BY s.status_title", new barChartMapper());
        return chartsTestList;
    }



}
