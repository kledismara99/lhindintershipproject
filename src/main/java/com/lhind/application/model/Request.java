package com.lhind.application.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;

@Entity
@Table(name = "request")
public class Request {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "request_id")
    private Integer id;

    @Column(name = "created_date_time")
    private Timestamp createdDateTime;

    @Column(name = "is_seen_by_admin")
    private Integer isSeenByAdmin;

    @OneToOne
    @JoinColumn(name = "trip_id")
    @JsonProperty
    Trip trip;

    public Request(Integer id, Timestamp createdDateTime, Trip trip) {
        this.id = id;
        this.createdDateTime = createdDateTime;
        this.trip = trip;
    }

    public Request() {
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Timestamp createdDateTime) {
        this.createdDateTime = createdDateTime;
    }


    public Integer getIsSeenByAdmin() {
        return isSeenByAdmin;
    }

    public void setIsSeenByAdmin(Integer isSeenByAdmin) {
        this.isSeenByAdmin = isSeenByAdmin;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }



}
