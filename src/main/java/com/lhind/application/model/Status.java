package com.lhind.application.model;


import javax.persistence.*;

/*
*
*    THIS MODEL HAVE STATUS OF A TRIP
*
*
*/


@Entity
@Table(name = "status")
public class Status {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "status_id")
    private Integer id;

    @Column(name = "status_title")
    private String title;

    public Status(Integer id, String title) {
        this.id = id;
        this.title = title;
    }

    public Status() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}
