package com.lhind.application.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "roles")
public class Role {


    @Id
    @Column(name = "role_id")
    private Integer id;

    @Column(name = "role_title")
    private String roleTitle;


    public Role(Integer id, String roleTitle) {
        this.id = id;
        this.roleTitle = roleTitle;
    }

    public Role(Integer id, String roleTitle, List<UserDetails> users) {
        this.id = id;
        this.roleTitle = roleTitle;
    }

    public Role() {
    }

    public Role(int role_id) {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleTitle() {
        return roleTitle;
    }

    public void setRoleTitle(String roleTitle) {
        this.roleTitle = roleTitle;
    }



    @Override
    public String toString() {
        return "RoleModel{" +
                "id=" + id +
                ", roleTitle='" + roleTitle + '\'' +
                '}';
    }
}
