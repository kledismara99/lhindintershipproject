package com.lhind.application.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "flights")
public class Flight {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "flight_id")
    private Integer id;

    @Column(name = "created_time_date")
    private Timestamp createdDateTime;

    @Column(name = "is_deleted")
    private Integer isDeleted;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "trip_id")
    @JsonProperty
    private Trip trip;

    public Flight() {
    }

    public Flight(Integer id, Timestamp createdDateTime, Integer isDeleted, Trip trip) {
        this.id = id;
        this.createdDateTime = createdDateTime;
        this.isDeleted = isDeleted;
        this.trip = trip;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Timestamp createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", createdDateTime=" + createdDateTime +
                ", isDeleted=" + isDeleted +
                ", trip=" + trip +
                '}';
    }
}
