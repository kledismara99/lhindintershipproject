package com.lhind.application.model;


import javax.persistence.*;

@Entity
@Table(name = "travellingreasons")
public class Reason {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "reason_id")
    private Integer id;

    @Column(name = "reason_title")
    private String reasonTitle;

    public Reason() {
    }

    public Reason(Integer id, String reasonTitle) {
        this.id = id;
        this.reasonTitle = reasonTitle;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReasonTitle() {
        return reasonTitle;
    }

    public void setReasonTitle(String reasonTitle) {
        this.reasonTitle = reasonTitle;
    }
}
