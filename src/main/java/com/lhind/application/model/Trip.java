package com.lhind.application.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.context.annotation.EnableMBeanExport;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "trip")
public class Trip {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "trip_id")
    private Integer id;

    @Column(name = "trip_description")
    @NotNull
    private String tripDescription;

    @Column(name = "from_place")
    @NotNull
    private String fromPlace;

    @Column(name = "to_place")
    @NotNull
    private String toPlace;

    @Column(name = "departure_date")
    @NotNull
    private LocalDate departureDate;

    @Column(name = "arrival_date")
    @NotNull
    private LocalDate arrivalDate;

    @Column(name = "is_deleted")
    private Integer isDeleted;

    @ManyToOne
    @JoinColumn(name = "status_id")
    @JsonProperty
    private Status status;

    @OneToOne(mappedBy = "trip")
    @JsonIgnore
    private Request request;

    @ManyToOne
    @JoinColumn(name = "reason_id")
    @JsonProperty
    private Reason reason;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    @JsonProperty
    private UserDetails user;


    @OneToMany(mappedBy = "trip")
    @JsonIgnore
    private List<Flight> flightList;



    public Trip() {
    }

    public Trip(Integer id, String tripDescription, String fromPlace, String toPlace, LocalDate departureDate, LocalDate arrivalDate, Status status, UserDetails user, Integer isDeleted) {
        this.id = id;
        this.tripDescription = tripDescription;
        this.fromPlace = fromPlace;
        this.toPlace = toPlace;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.status = status;
        this.user = user;
        this.isDeleted=isDeleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTripDescription() {
        return tripDescription;
    }

    public void setTripDescription(String tripDescription) {
        this.tripDescription = tripDescription;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getToPlace() {
        return toPlace;
    }

    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public UserDetails getUser() {
        return user;
    }

    public void setUser(UserDetails user) {
        this.user = user;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public List<Flight> getFlightList() {
        return flightList;
    }

    public void setFlightList(List<Flight> flightList) {
        this.flightList = flightList;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", tripDescription='" + tripDescription + '\'' +
                ", fromPlace='" + fromPlace + '\'' +
                ", toPlace='" + toPlace + '\'' +
                ", departureDate=" + departureDate +
                ", arrivalDate=" + arrivalDate +
                ", status=" + status +
                ", user=" + user +
                '}';
    }
}
