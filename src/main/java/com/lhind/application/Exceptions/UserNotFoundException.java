package com.lhind.application.Exceptions;

public class UserNotFoundException extends Exception{

    String message;

    public UserNotFoundException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return "UserNotFoundException{" +
                "message='" + message + '\'' +
                '}';
    }
}
