package com.lhind.application.service.userService;

import com.lhind.application.model.UserDetails;

import java.util.List;

public interface UserService {
    List<UserDetails> searchUsers(String name);
    List<UserDetails> getAllUsers();
    List<UserDetails> getAllDeletedUsers();
    UserDetails getUserById(Integer id);
    String saveUser(UserDetails user);
    String updateUser(Integer id);
    String deleteUser(Integer id);
}
