package com.lhind.application.service.statusService;

import com.lhind.application.model.Status;

import java.util.List;

public interface StatusService {
    Status addNewStatus(Status status);
    void updateStatus(Status status);
    void deleteStatus(Status status);
    List<Status> getAllStatus();
}
