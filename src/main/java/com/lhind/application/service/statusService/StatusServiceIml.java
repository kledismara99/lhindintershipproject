package com.lhind.application.service.statusService;

import com.lhind.application.model.Status;
import com.lhind.application.repository.statusRepository.StatusRepositoryIml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceIml implements StatusService{

    @Autowired
    StatusRepositoryIml repo;

    @Override
    public Status addNewStatus(Status status) {
       return repo.addNewStatus(status);
    }

    @Override
    public void updateStatus(Status status) {
        repo.updateStatus(status);
    }

    @Override
    public void deleteStatus(Status status) {
        repo.deleteStatus(status);
    }

    @Override
    public List<Status> getAllStatus() {
        return repo.getAllStatus();
    }
}
