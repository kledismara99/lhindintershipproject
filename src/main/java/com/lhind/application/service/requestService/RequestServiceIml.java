package com.lhind.application.service.requestService;

import com.lhind.application.model.Request;
import com.lhind.application.repository.requestRepository.RequestRepositoryIml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class RequestServiceIml implements RequestService{

    @Autowired
    RequestRepositoryIml requestRepositoryIml;

    @Override
    public Integer saveRequest(Request request) {
        return requestRepositoryIml.saveRequest(request);
    }

    @Override
    public List<Request> getAllRequests() {
        return requestRepositoryIml.getAllRequests();
    }

    @Override
    public Request getARequest(Integer id) {
        return requestRepositoryIml.getARequest(id);
    }

    @Override
    public Integer updateIsSeenByAdmin(Integer id) {
        return requestRepositoryIml.updateIsSeenByAdmin(id);
    }

    @Override
    public Integer acceptRequest(Integer id) {
        return requestRepositoryIml.acceptRequest(id);
    }

    @Override
    public String deleteRequest(Integer id) {
        return requestRepositoryIml.deleteRequest(id);
    }

    @Override
    public List<Request> getAllAcceptedRequests() {
        return requestRepositoryIml.getAllAcceptedRequests();
    }

    @Override
    public List<Request> getAllWaitingRequests() {
        return requestRepositoryIml.getAllWaitingRequests();
    }

    @Override
    public List<Request> getAllDeletedRequests() {
        return requestRepositoryIml.getAllDeletedRequests();
    }

    @Override
    public BigInteger getAllNotSeenRequests() {
        return requestRepositoryIml.getAllNotSeenRequests();
    }

    @Override
    public List<Request> getAllRequestsByUserId(Integer id) {
        return requestRepositoryIml.getAllRequestsByUserId(id);
    }

    @Override
    public List<Request> getAllRequestSearchedByUsername(String name) {
        return requestRepositoryIml.getAllRequestSearchedByUsername(name);
    }
}
