package com.lhind.application.service.adminService;

import com.lhind.application.model.UserDetails;
import com.lhind.application.repository.adminRepository.AdminRepositoryIml;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.*;

@Service
public class AdminServiceIml implements AdminService, UserDetailsService{


    @Autowired
    AdminRepositoryIml adminRepositoryIml;

    @Override
    public List<UserDetails> searchUsers(String name) {
        return adminRepositoryIml.searchUsers(name);
    }

    @Override
    public List<UserDetails> getAllUsers() {
        return adminRepositoryIml.getAllUsers();
    }

    @Override
    public List<UserDetails> getAllDeletedUsers() {
        return adminRepositoryIml.getAllDeletedUsers();
    }

    @Override
    public UserDetails getUserById(Integer id) {
        return  adminRepositoryIml.getUserById(id);
    }

    @Override
    public Integer saveUser(UserDetails user) {
         return adminRepositoryIml.saveUser(user);
    }

    @Override
    public Integer updateUser(UserDetails user) {
        return adminRepositoryIml.updateUser(user);
    }

    @Override
    public Integer deleteUser(Integer id) {
        return adminRepositoryIml.deleteUser(id);
    }

    @Override
    public UserDetails getUserByUsername(String username) {
        return adminRepositoryIml.getUserByUsername(username);
    }

    @Override
    public Integer activateUser(Integer id) {
       return adminRepositoryIml.activateUser(id);
    }

    @Override
    public BigInteger getNumberOfAllUsers() {
        return adminRepositoryIml.getNumberOfAllUsers();
    }


    @Override
    public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDetails user = getUserByUsername(s);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().getRoleTitle()));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }
}
