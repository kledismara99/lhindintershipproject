package com.lhind.application.service.adminService;

import com.lhind.application.model.UserDetails;

import java.math.BigInteger;
import java.util.List;

public interface AdminService{
    List<UserDetails> searchUsers(String name);
    List<UserDetails> getAllUsers();
    List<UserDetails> getAllDeletedUsers();
    UserDetails getUserById(Integer id);
    Integer saveUser(UserDetails user);
    Integer updateUser(UserDetails user);
    Integer deleteUser(Integer id);
    UserDetails getUserByUsername(String username);
    Integer activateUser(Integer id);
    BigInteger getNumberOfAllUsers();

}
