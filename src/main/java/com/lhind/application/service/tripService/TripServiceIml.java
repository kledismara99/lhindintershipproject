package com.lhind.application.service.tripService;

import com.lhind.application.model.Trip;
import com.lhind.application.modelMappings.barChartMapper;
import com.lhind.application.modelMappings.chartsTest;
import com.lhind.application.repository.tripRepository.TripRepositoryIml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class TripServiceIml implements TripService{

    @Autowired
    TripRepositoryIml tripRepositoryIml;


    @Override
    public Integer addNewTrip(String trip_description, String from_place, String to_place, LocalDate departure_date, LocalDate arrival_date, Integer status_id, Integer reason_id, Integer user_id) {
        return tripRepositoryIml.addNewTrip(trip_description, from_place, to_place, departure_date, arrival_date, status_id, reason_id, user_id);
    }

    @Override
    public Trip getTripById(Integer id) {
        return tripRepositoryIml.getTripById(id);
    }

    @Override
    public List<Trip> getTripsFromUserId(Integer id) {
        return tripRepositoryIml.getTripsFromUserId(id);
    }

    @Override
    public List<Trip> getAllTrips() {
        return tripRepositoryIml.getAllTrips();
    }

    @Override
    public Integer updateTripById(String trip_description, String from_place, String to_place, Date departure_date, Date arrival_date, Integer status_id, Integer user_id, Integer id) {
        return tripRepositoryIml.updateTripById(trip_description, from_place, to_place, departure_date, arrival_date, status_id, user_id, id);
    }

    @Override
    public Integer activateTrip(Integer id) {
        return tripRepositoryIml.activateTrip(id);
    }

    @Override
    public Integer deleteTripById(Integer id) {
        return tripRepositoryIml.deleteTripById(id);
    }

    @Override
    public Integer changeTripStatus(Integer status_id, Integer trip_id) {
        return tripRepositoryIml.changeTripStatus(status_id, trip_id);
    }

    @Override
    public BigInteger getNumberOfTrips() {
        return tripRepositoryIml.getNumberOfTrips();
    }

    @Override
    public BigInteger getNumberOfWaitingTrips() {
        return tripRepositoryIml.getNumberOfWaitingTrips();
    }

    @Override
    public List<chartsTest> getNumberOfReasonsOfTripsGroupedByReason() {
        return tripRepositoryIml.getNumberOfReasonsOfTripsGroupedByReason();
    }

    @Override
    public List<barChartMapper> getNumberOfStatusOfTripsGroupedByStatus() {
        return tripRepositoryIml.getNumberOfStatusOfTripsGroupedByStatus();
    }

    @Override
    public BigInteger getNumberOfTripsByUserId(Integer id) {
        return tripRepositoryIml.getNumberOfTripsByUserId(id);
    }

    @Override
    public BigInteger getNumberOfWaitingTripsByUserId(Integer id) {
        return tripRepositoryIml.getNumberOfWaitingTripsByUserId(id);
    }

    @Override
    public BigInteger getNumberOfCreatedTripsByUserId(Integer id) {
        return tripRepositoryIml.getNumberOfCreatedTripsByUserId(id);
    }

}
