package com.lhind.application.service.tripService;


import com.lhind.application.model.Trip;
import com.lhind.application.modelMappings.barChartMapper;
import com.lhind.application.modelMappings.chartsTest;
import org.apache.tomcat.jni.Local;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface TripService {

    Integer addNewTrip(String trip_description,
                       String from_place,
                       String to_place,
                       LocalDate departure_date,
                       LocalDate arrival_date,
                       Integer status_id,
                       Integer reason_id,
                       Integer user_id);
    Trip getTripById(Integer id);
    List<Trip> getTripsFromUserId(Integer id);
    List<Trip> getAllTrips();
    Integer updateTripById(String trip_description,
                           String from_place,
                           String to_place,
                           Date departure_date,
                           Date arrival_date,
                           Integer status_id,
                           Integer user_id,
                           Integer id);
    Integer activateTrip(Integer id);
    Integer deleteTripById(Integer id);
    Integer changeTripStatus(Integer status_id, Integer trip_id);
    BigInteger getNumberOfTrips();
    BigInteger getNumberOfWaitingTrips();
    List<chartsTest> getNumberOfReasonsOfTripsGroupedByReason();
    List<barChartMapper> getNumberOfStatusOfTripsGroupedByStatus();
    BigInteger getNumberOfTripsByUserId(Integer id);
    BigInteger getNumberOfWaitingTripsByUserId(Integer id);
    BigInteger getNumberOfCreatedTripsByUserId(Integer id);
}
