package com.lhind.application.service.flightService;

import com.lhind.application.model.Flight;
import com.lhind.application.repository.flightRepository.FlightRepositoryIml;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class FlightServiceIml implements FlightService{


    @Autowired
    FlightRepositoryIml flightRepositoryIml;

    @Override
    public Integer addFlight(Flight flight) {
       return flightRepositoryIml.addFlight(flight);
    }

    @Override
    public List<Flight> getFlightsByUserId(Integer id) {
        return flightRepositoryIml.getFlightsByUserId(id);
    }

    @Override
    public List<Flight> getAllFlights() {
      return flightRepositoryIml.getAllFlights();
    }

    @Override
    public Flight getFlightById(Integer id) {
      return flightRepositoryIml.getFlightById(id);
    }

    @Override
    public List<Flight> getAllFlightsByTripId(Integer id) {
       return flightRepositoryIml.getAllFlightsByTripId(id);
    }

    @Override
    public Integer deleteFlight(Integer flightId) {
      return flightRepositoryIml.deleteFlight(flightId);
    }

    @Override
    public BigInteger getNumberOfAllFlights() {
        return flightRepositoryIml.getNumberOfAllFlights();

    }

    @Override
    public BigInteger getNumberOfAllFlightsByUserId(Integer userId) {
        return flightRepositoryIml.getNumberOfAllFlightsByUserId(userId);
    }
}
