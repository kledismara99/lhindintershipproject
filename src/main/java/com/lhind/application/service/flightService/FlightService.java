package com.lhind.application.service.flightService;

import com.lhind.application.model.Flight;

import java.math.BigInteger;
import java.util.List;

public interface FlightService {

    Integer addFlight(Flight flight);
    List<Flight> getFlightsByUserId(Integer id);
    List<Flight> getAllFlights();
    Flight getFlightById(Integer id);
    List<Flight> getAllFlightsByTripId(Integer id);
    Integer deleteFlight(Integer flightId);
    BigInteger getNumberOfAllFlights();
    BigInteger getNumberOfAllFlightsByUserId(Integer userId);
}
